# DECaxp project now builds using CMake

The DECaxp project has been converted to a CMake project.  As such, the bld directory and Makefile are no longer part of this project.  The code compiles, but some of the errors others have seen in the test code remain.  I will be getting to these shortly.

Some of the tools that I am using are:

1. Cygwin 3.0.7
1. CMake 3.6.2
1. GCC 7.4.0
1. WinPcap 4.1.3
1. WpdPack 4.1.2

I'll add to this, as I go.