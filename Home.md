Welcome to the DECaxp wiki!

I have been away from this for a while.  I've been working on an open version of SDL, called OpenSDL.  This is nearing completion.  When done, I have a couple of things I want to change in here.

1. I want to completely redo all the data structures into SDL format.
1. I want to redo the branch prediction.  This was the first code I wrote and think I can do better.
1. I'm not happy with the cache implementations.  I think these need to be refactored.
1. The system code is going to be replaced.  I have been reading some documentation and want to start with a clean sheet on this one.
1. I'm sure there is much more.

Thanks for your patience.

~Jon.
